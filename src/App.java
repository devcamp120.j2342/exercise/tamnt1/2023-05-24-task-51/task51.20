
import java.util.ArrayList;
import java.util.Iterator;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1();
        subTask2();
        subTask3();
        subTask4();
        subTask5();
        subTask6();
        subTask7();
        subTask8();
        subTask9();
        subTask10();
        subTask11();
        subTask12();
        subTask13();
    }

    public static void subTask1() {
        ArrayList<String> colorList = new ArrayList<>();
        // Thêm 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");
        // In ra danh sách màu sắc
        System.out.println(colorList);
    }

    public static void subTask2() {
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        // Thêm 3 số vào mỗi ArrayList
        list1.add(1);
        list1.add(2);
        list1.add(3);

        list2.add(4);
        list2.add(5);
        list2.add(6);

        // Cộng các giá trị của list1 vào list2
        for (int i = 0; i < list1.size(); i++) {
            int sum = list2.get(i) + list1.get(i);
            list2.set(i, sum);
        }

        // In ra ArrayList được cộng thêm
        System.out.println(list2);
    }

    public static void subTask3() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // In ra số lượng phần tử của ArrayList
        int size = colorList.size();
        System.out.println("Số lượng phần tử của ArrayList: " + size);
    }

    public static void subTask4() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // Lấy và in ra phần tử thứ 4 của ArrayList
        String element = colorList.get(3);
        System.out.println("Phần tử thứ 4 của ArrayList: " + element);
    }

    public static void subTask5() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm tối đa 5 màu sắc vào danh sách
        if (colorList.size() < 5) {
            colorList.add("Red");
            colorList.add("Green");
            colorList.add("Blue");
            colorList.add("Yellow");
            colorList.add("Orange");
        }

        // Lấy và in ra phần tử cuối cùng của ArrayList
        String lastElement = colorList.get(colorList.size() - 1);
        System.out.println("Phần tử cuối cùng của ArrayList: " + lastElement);
    }

    public static void subTask6() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm tối đa 5 màu sắc vào danh sách
        if (colorList.size() < 5) {
            colorList.add("Red");
            colorList.add("Green");
            colorList.add("Blue");
            colorList.add("Yellow");
            colorList.add("Orange");
        }

        // In ra ArrayList vừa tạo
        System.out.println("ArrayList vừa tạo: " + colorList);

        // Xóa phần tử cuối cùng của ArrayList
        if (!colorList.isEmpty()) {
            colorList.remove(colorList.size() - 1);
        }

        // In lại ArrayList sau khi xóa phần tử cuối cùng
        System.out.println("ArrayList sau khi xóa: " + colorList);
    }

    public static void subTask7() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // Sử dụng vòng lặp forEach để in từng phần tử ra terminal
        colorList.forEach(color -> System.out.println(color));

    }

    public static void subTask8() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // Sử dụng Iterator để duyệt qua từng phần tử và in giá trị ra terminal
        Iterator<String> iterator = colorList.iterator();
        while (iterator.hasNext()) {
            String color = iterator.next();
            System.out.println(color);
        }
    }

    public static void subTask9() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // Sử dụng vòng lặp for để duyệt qua từng phần tử và in giá trị ra terminal
        for (String color : colorList) {
            System.out.println(color);
        }
    }

    public static void subTask10() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm tối đa 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // Ghi ra terminal ArrayList vừa tạo
        System.out.println("ArrayList ban đầu:");
        System.out.println(colorList);

        // Thêm một màu sắc vào đầu của ArrayList
        colorList.add(0, "Purple");

        // Ghi lại ra terminal ArrayList sau khi thêm màu sắc vào đầu
        System.out.println("ArrayList sau khi thêm màu sắc vào đầu:");
        System.out.println(colorList);
    }

    public static void subTask11() {
        ArrayList<String> colorList = new ArrayList<>();

        // Thêm tối đa 5 màu sắc vào danh sách
        colorList.add("Red");
        colorList.add("Green");
        colorList.add("Blue");
        colorList.add("Yellow");
        colorList.add("Orange");

        // Ghi ra terminal ArrayList vừa tạo
        System.out.println("ArrayList ban đầu:");
        System.out.println(colorList);

        // Sửa màu của phần tử thứ 3 thành màu vàng
        colorList.set(2, "Yellow");

        // Ghi lại ra terminal ArrayList sau khi sửa màu
        System.out.println("ArrayList sau khi sửa màu:");
        System.out.println(colorList);
    }

    public static void subTask12() {
        ArrayList<String> nameList = new ArrayList<>();

        // Thêm các giá trị vào danh sách
        nameList.add("Tam");
        nameList.add("Nam");
        nameList.add("Phu");
        nameList.add("Le");
        nameList.add("Dung");
        nameList.add("Hoai");
        nameList.add("Kha");

        // Ghi ra terminal vị trí đầu tiên của phần tử "Dung"
        int dungIndex = nameList.indexOf("Dung");
        System.out.println("Vị trí đầu tiên của phần tử 'Dung' là: " + dungIndex);

        // Ghi ra terminal vị trí đầu tiên của phần tử "Tam"
        int tamIndex = nameList.indexOf("Tam");
        System.out.println("Vị trí đầu tiên của phần tử 'Tam' là: " + tamIndex);
    }

    public static void subTask13() {
        ArrayList<String> nameList = new ArrayList<>();

        // Thêm các giá trị vào danh sách
        nameList.add("Tam");
        nameList.add("Nam");
        nameList.add("Phu");
        nameList.add("Le");
        nameList.add("Dung");
        nameList.add("Hoai");
        nameList.add("Kha");

        // Ghi ra terminal vị trí cuối cùng của phần tử "Dung"
        int dungIndex = nameList.lastIndexOf("Dung");
        System.out.println("Vị trí cuối cùng của phần tử 'Dung' là: " + dungIndex);

        // Ghi ra terminal vị trí cuối cùng của phần tử "Tam"
        int tamIndex = nameList.lastIndexOf("Tam");
        System.out.println("Vị trí cuối cùng của phần tử 'Tam' là: " + tamIndex);
    }

}
